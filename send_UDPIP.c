#include <stdio.h>
#include <pthread.h>
#include <mqueue.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "inverse_kinematics.h"
#include "send_UDPIP.h"
#include "periodic.h"

#define MAXLEN 100

/* SendUDPIP thread function prototype */
void *tsendUDPIPThreadFunc(void *);

/* Thread variable */
pthread_t tISRThread;
/* Thread attributes structure */
pthread_attr_t aISRThreadAttr;

int init_send_UDPIP() {

	int status;

	/* Initialize attributes structure */
	pthread_attr_init(&aISRThreadAttr);

	/* Start thread */
	if ((status = pthread_create(&tISRThread, NULL, tsendUDPIPThreadFunc, &aISRThreadAttr))) {
		fprintf(stderr, "Cannot create thread.\n");
		return 0;
	}

	printf("Send Threat Active!\n");
	fflush(stdout);

	return 1;

}

void *tsendUDPIPThreadFunc(void *cookie) {

	/* Scheduling policy: FIFO or RR */
	int policy = SCHED_FIFO;
	/* Structure of other thread parameters */
	struct sched_param param;

	/* Read modify and set new thread priority */
	param.sched_priority = sched_get_priority_max(policy) - 4;
	pthread_setschedparam( pthread_self(), policy, &param);

	while(1){
		char buff[MAXLEN]; 
		int n, addr_length;
		/* Socket address structure */
		struct sockaddr_in socket_addr;
		/* Create socket */
		int my_socket = socket(PF_INET, SOCK_DGRAM, 0);

		if (my_socket == -1) {
			fprintf(stderr, "Cannot create socket\n");
			return 0;
		}

		/* Initialize socket address to 0*/
		memset(&socket_addr, 0, sizeof(socket_addr));
		/* Set socket address parameters */
		socket_addr.sin_family = AF_INET;
		socket_addr.sin_port = htons(1100);
		socket_addr.sin_addr.s_addr = INADDR_ANY;
		char charray[19];
		charray[19] = '\n';

		pthread_rwlock_rdlock(&calc_mtr_ang_mutex);
		snprintf(charray, 19, "%06d%06d%06d", (int)abs(1000*calc_ang.mtr_1),
									(int)abs(1000*calc_ang.mtr_2),
									(int)abs(1000*calc_ang.mtr_3));
		
		charray[5]  = '+';
		charray[11] = '+';
		charray[17] = '+';

		if(calc_ang.mtr_1<0){
			charray[5] = '-';
		};
		if(calc_ang.mtr_2<0){
			charray[11] = '-';
		};
		if(calc_ang.mtr_3<0){
			charray[17] = '-';
		};
		pthread_rwlock_unlock(&calc_mtr_ang_mutex);

		sendto(my_socket, charray, 18, MSG_CONFIRM, (const struct sockaddr *) &socket_addr, sizeof(socket_addr)); 

		usleep(200000);

	}

	pthread_barrier_wait(&barrier);
	return EXIT_SUCCESS;
}
