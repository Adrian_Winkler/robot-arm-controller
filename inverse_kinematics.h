#ifndef INVERSE_KINEMATICS_H_
#define INVERSE_KINEMATICS_H_

extern pthread_barrier_t barrier;

struct _robot_angles {
	double th1;
	double th2;
	double th3;
};

struct _motors_params {
	double k;
	double T;
	double ts;
};

typedef struct _motors_params _motors_params;

extern _motors_params motors_params;

typedef struct _robot_angles _robot_angles;

struct _robot_angles robot_angles;

typedef struct _setpoint {

	double mtr_1;
	double mtr_2;
	double mtr_3;

}_setpoint;

extern _setpoint setpoint;

extern double set_val_mtr_1;
extern double set_val_mtr_2;
extern double set_val_mtr_3;

/* Function creates screen thread */
int init_inverse_kinematics();

_robot_angles calc_inv_kin(double X, double Y, double Z);

extern pthread_mutex_t set_val_mtr_mutex;
extern pthread_mutex_t set_val_mtr_2_mutex;
extern pthread_mutex_t set_val_mtr_3_mutex;

#endif /* INVERSE_KINEMATICS_H_ */