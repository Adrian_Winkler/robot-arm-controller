#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <math.h>
#include <mqueue.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <string.h>
#include <unistd.h>


#include "inverse_kinematics.h"
#include "PID.h"
#include "periodic.h"
#include "motors.h"
#include "receive_TCPIP.h"


#define MAXLEN 100

_PID_params PID = {1.0, 1.0, 1.0};


/* Semaphores for global variables */
pthread_mutex_t set_val_mtr_mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t PID_params_mutex = PTHREAD_MUTEX_INITIALIZER;


/* Internal functions prototypes */
double calc_PID(double err_tab[3], double prev_u);

/**
 * Function initializes PID module
 */
int initialize_PID() {

    calc_PID_mtr_1 = 0;
    calc_PID_mtr_2 = 0;
    calc_PID_mtr_3 = 0;
	
	printf("PID Threat Active!\n");
	fflush(stdout);
    
	return 0;
}

/**
 * Function calculates one PID step
 */
int calculate_PID() {

/* Set new values calculated in previous step*/

	pthread_mutex_lock(&set_PID_mtr_mutex);
	set_value.PID_mtr_1 = calc_PID_mtr_1;
	set_value.PID_mtr_2 = calc_PID_mtr_2;
	set_value.PID_mtr_3 = calc_PID_mtr_3;
	pthread_mutex_unlock(&set_PID_mtr_mutex);

/* Get set values of angles from inverse kinematics*/

	pthread_mutex_lock(&set_val_mtr_mutex);
	double tmp_set_val_1 = setpoint.mtr_1;
	double tmp_set_val_2 = setpoint.mtr_2;
	double tmp_set_val_3 = setpoint.mtr_3;
	pthread_mutex_unlock(&set_val_mtr_mutex);

/* Get present values of angles of motors*/

	pthread_rwlock_rdlock(&calc_mtr_ang_mutex);
		double tmp_calc_ang_1 = calc_ang.mtr_1;
		double tmp_calc_ang_2 = calc_ang.mtr_2;
		double tmp_calc_ang_3 = calc_ang.mtr_3;
	pthread_rwlock_unlock(&calc_mtr_ang_mutex);

    double error_PID_1 = tmp_set_val_1 - tmp_calc_ang_1;
    double error_PID_2 = tmp_set_val_2 - tmp_calc_ang_2;
    double error_PID_3 = tmp_set_val_3 - tmp_calc_ang_3;

	error_table_1[2] = error_table_1[1];
	error_table_1[1] = error_table_1[0];
	error_table_1[0] = error_PID_1;

	error_table_2[2] = error_table_2[1];
	error_table_2[1] = error_table_2[0];
	error_table_2[0] = error_PID_2;

	error_table_3[2] = error_table_3[1];
	error_table_3[1] = error_table_3[0];
	error_table_3[0] = error_PID_3;

	/* Calculate next set of PID */
	calc_PID_mtr_1 = calc_PID(error_table_1, -calc_PID_mtr_1_prev);

		pthread_mutex_lock(&synch_mutex);
			int flag_tmp = flag_synchronous;	
		pthread_mutex_unlock(&synch_mutex);

	if(!flag_tmp){
		if(abs(tmp_calc_ang_1-tmp_set_val_1)<10){
			calc_PID_mtr_2 = calc_PID(error_table_2, -calc_PID_mtr_2_prev);
		}
		if(abs(tmp_calc_ang_2-tmp_set_val_2)<10){
			calc_PID_mtr_3 = calc_PID(error_table_3, -calc_PID_mtr_3_prev);
		}
	}
	else{
		calc_PID_mtr_2 = calc_PID(error_table_2, -calc_PID_mtr_2_prev);
		calc_PID_mtr_3 = calc_PID(error_table_3, -calc_PID_mtr_3_prev);
	}


	pthread_rwlock_rdlock(&calc_mtr_ang_mutex);
		calc_PID_mtr_1_prev = calc_ang.mtr_1;
		calc_PID_mtr_2_prev = calc_ang.mtr_2;
		calc_PID_mtr_3_prev = calc_ang.mtr_3;
	pthread_rwlock_unlock(&calc_mtr_ang_mutex);


	return 0;
}

/**
 * Calculate of PID control
 */
double calc_PID(double err_tab[3], double prev_u) {

	pthread_mutex_lock(&PID_params_mutex);
	double calc_pid = prev_u + PID.P*err_tab[0] - PID.P*err_tab[1] + PID.P*PID.I*err_tab[1] + PID.P*PID.D*(err_tab[0] - 2*err_tab[1] + err_tab[2]);
	pthread_mutex_unlock(&PID_params_mutex);
	
	return calc_pid;
}

