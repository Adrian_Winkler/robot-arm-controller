#ifndef MOTORS_H_
#define MOTORS_H_

#define INTERVAL 0.9

/* Function initializes periodic thread */
int initialize_PID();

/* Parameters struct */
struct _PID_params {
	double P;
	double D;
	double I;
};

typedef struct _PID_params _PID_params;

/* Make this vars public */
extern _PID_params PID;

static double calc_PID_mtr_1;
static double calc_PID_mtr_2;
static double calc_PID_mtr_3;

static double calc_PID_mtr_1_prev;
static double calc_PID_mtr_2_prev;
static double calc_PID_mtr_3_prev;

static double error_table_1[3] = {0.0, 0.0, 0.0};
static double error_table_2[3] = {0.0, 0.0, 0.0};
static double error_table_3[3] = {0.0, 0.0, 0.0};

extern pthread_mutex_t set_val_mtr_mutex;
extern pthread_mutex_t set_PID_mtr_mutex;
extern pthread_mutex_t PID_params_mutex;

/* One step calculation function */
int calculate_PID(void);


#endif /* MOTORS_H_ */