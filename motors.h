#ifndef MOTORS_H_
#define MOTORS_H_

#define INTERVAL 0.1

/* Function initializes periodic thread */
int initialize_motors();

static double calc_mtr_spd_1;
static double calc_mtr_spd_2;
static double calc_mtr_spd_3;

static double calc_mtr_ang_1_tmp;
static double calc_mtr_ang_2_tmp;
static double calc_mtr_ang_3_tmp;


/* One step calculation function */
int calculate_motors(void);

double my_rand();

#endif /* MOTORS_H_ */