#include <gtk/gtk.h>
#include "gtk.h"

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <math.h>
#include <signal.h>

#include <semaphore.h>
#include "receive_UDP.h"

#define MAXLEN 100

struct robot_properties Robot_Params = {0.0, 0.0, 0.0, 10.0, 10.0, 10.0, 10.0};

sem_t mySemaphore;
pthread_barrier_t barrier_GUI;


/* To create this GUI I used some examples found on the Internet, for example: I am not a author of clocks */

void DrawTickAt (GtkWidget *widget, cairo_t *cr, int nHour, int cx, int cy)
{
   double dRadians = nHour * 3.14 / 18.0;

   int x1 = cx+(int) ((0.95 * radius * sin (dRadians)));
   int y1 = cy-(int) ((0.95 * radius * cos (dRadians)));
   int x2 = cx+(int) ((1.0 * radius * sin (dRadians)));
   int y2 = cy-(int) ((1.0 * radius * cos (dRadians)));

   cairo_set_source_rgb(cr, WHITE);
   cairo_set_line_width(cr, 2);
   cairo_move_to(cr, x1, y1);
   cairo_line_to(cr, x2, y2);
   cairo_stroke(cr);
}


void DrawAxisX (GtkWidget *widget, cairo_t *cr, int cx, int cy)
{
   
   float dRadians = Robot_Params.XAxis * 3.14 / 180.0;
   cairo_set_source_rgb(cr, RED);
   cairo_set_line_width(cr, WIDTH);
   cairo_move_to(cr, cx, cy);
   cairo_line_to(cr, 
                   cx + (0.9 * radius * sin (dRadians)),
                   cy - (0.9 * radius * cos (dRadians)));
   cairo_stroke(cr);
}

void DrawAxisY (GtkWidget *widget, cairo_t *cr, int cx, int cy)
{
   
   float dRadians = Robot_Params.YAxis * 3.14 / 180.0;
   cairo_set_source_rgb(cr, RED);
   cairo_set_line_width(cr, WIDTH);
   cairo_move_to(cr, cx, cy);
   cairo_line_to(cr, 
                   cx + (0.9 * radius * sin (dRadians)),
                   cy - (0.9 * radius * cos (dRadians)));
   cairo_stroke(cr);
}

void DrawAxisZ (GtkWidget *widget, cairo_t *cr, int cx, int cy)
{
   
   float dRadians = Robot_Params.ZAxis * 3.14 / 180.0;
   cairo_set_source_rgb(cr, RED);
   cairo_set_line_width(cr, WIDTH);
   cairo_move_to(cr, cx, cy);
   cairo_line_to(cr, 
                   cx + (0.9 * radius * sin (dRadians)),
                   cy - (0.9 * radius * cos (dRadians)));
   cairo_stroke(cr);
}

void DrawAxisSpeed (GtkWidget *widget, cairo_t *cr, int cx, int cy)
{
   
   float dRadians = Robot_Params.Speed * 3.14 / 15.0;
   cairo_set_source_rgb(cr, RED);
   cairo_set_line_width(cr, WIDTH);
   cairo_move_to(cr, cx, cy);
   cairo_line_to(cr, 
                   cx + (0.9 * radius * sin (dRadians)),
                   cy - (0.9 * radius * cos (dRadians)));
   cairo_stroke(cr);
}


void DrawAxisX_actu (GtkWidget *widget, cairo_t *cr, int cx, int cy)
{
   sem_wait(&mySemaphore);
   double Xax = Robot_Actual_Params.XAxis;
   sem_post(&mySemaphore);
   if(Xax < TH1_MAX){
      GdkRGBA colorX;
      colorX.red = 0.0;
      colorX.green = 0.0;
      colorX.blue = 0.8;
      colorX.alpha = 0.25;
      gtk_widget_override_background_color(oveXEntry, GTK_STATE_NORMAL, &colorX);
   }

   float dRadians = (Xax * 3.14 / 180.0);
   

   cairo_set_source_rgb(cr, WHITE);
   cairo_set_line_width(cr, WIDTH * 2);
   cairo_move_to(cr, cx, cy);
   cairo_line_to(cr, 
                   cx + (0.9 * radius * sin (dRadians)),
                   cy - (0.9 * radius * cos (dRadians)));
   cairo_stroke(cr);
}

void DrawAxisY_actu (GtkWidget *widget, cairo_t *cr, int cx, int cy)
{
   sem_wait(&mySemaphore);
   double Yax = Robot_Actual_Params.YAxis;
   sem_post(&mySemaphore);
   if(Yax < TH1_MAX){
      GdkRGBA colorY;
      colorY.red = 0.0;
      colorY.green = 0.0;
      colorY.blue = 0.8;
      colorY.alpha = 0.25;
      gtk_widget_override_background_color(oveYEntry, GTK_STATE_NORMAL, &colorY);
   }

   float dRadians = (Yax * 3.14 / 180.0);

   cairo_set_source_rgb(cr, WHITE);
   cairo_set_line_width(cr, WIDTH * 2);
   cairo_move_to(cr, cx, cy);
   cairo_line_to(cr, 
                   cx + (0.9 * radius * sin (dRadians)),
                   cy - (0.9 * radius * cos (dRadians)));
   cairo_stroke(cr);
}

void DrawAxisZ_actu (GtkWidget *widget, cairo_t *cr, int cx, int cy)
{
   sem_wait(&mySemaphore);
   double Zax = Robot_Actual_Params.ZAxis;
   sem_post(&mySemaphore);

   if(Zax < TH1_MAX){
      GdkRGBA colorZ;
      colorZ.red = 0.0;
      colorZ.green = 0.0;
      colorZ.blue = 0.8;
      colorZ.alpha = 0.25;
      gtk_widget_override_background_color(oveZEntry, GTK_STATE_NORMAL, &colorZ);
   }

   float dRadians = (Zax * 3.14 / 180.0);
   sem_post(&mySemaphore);

   cairo_set_source_rgb(cr, WHITE);
   cairo_set_line_width(cr, WIDTH * 2);
   cairo_move_to(cr, cx, cy);
   cairo_line_to(cr, 
                   cx + (0.9 * radius * sin (dRadians)),
                   cy - (0.9 * radius * cos (dRadians)));
   cairo_stroke(cr);
}

static gboolean draw_cb_X(GtkWidget *widget, cairo_t *cr, gpointer data)
{

   int midx = gtk_widget_get_allocated_width(widget)/2;
   int midy = gtk_widget_get_allocated_height(widget)/2;
   int nHour;

   for (nHour = 1; nHour <= 36; nHour++)
   {
        DrawTickAt (widget, cr, nHour, midx, midy);
   }

   cairo_set_source_rgb(cr, RED);
   cairo_set_font_size (cr, 30);
   cairo_move_to(cr, midx-(midx/2),midy/2);
   cairo_show_text(cr, buffer);

   cairo_set_source_rgb(cr, RED);
   radius = MIN (midx, midy) -2 ;
   cairo_move_to(cr, midx , 0);

    DrawAxisX (widget, cr, midx, midy);
    DrawAxisX_actu (widget, cr, midx, midy);

   return FALSE;
}


static gboolean draw_cb_Y(GtkWidget *widget, cairo_t *cr, gpointer data)
{
   int midx = gtk_widget_get_allocated_width(widget)/2;
   int midy = gtk_widget_get_allocated_height(widget)/2;
   int nHour;

   for (nHour = 1; nHour <= 36; nHour++)
   {
        DrawTickAt (widget, cr, nHour, midx, midy);
   }

   cairo_set_source_rgb(cr, RED);
   cairo_set_font_size (cr, 30);
   cairo_move_to(cr, midx-(midx/2),midy/2);
   cairo_show_text(cr, buffer);

   cairo_set_source_rgb(cr, RED);
   radius = MIN (midx, midy) -2 ;
   cairo_move_to(cr, midx , 0);

   DrawAxisY (widget, cr, midx, midy);
   DrawAxisY_actu (widget, cr, midx, midy);

   return FALSE;
}

static gboolean draw_cb_Z(GtkWidget *widget, cairo_t *cr, gpointer data)
{
   int midx = gtk_widget_get_allocated_width(widget)/2;
   int midy = gtk_widget_get_allocated_height(widget)/2;
   int nHour;

   for (nHour = 1; nHour <= 36; nHour++)
   {
        DrawTickAt (widget, cr, nHour, midx, midy);
   }

   cairo_set_source_rgb(cr, RED);
   cairo_set_font_size (cr, 30);
   cairo_move_to(cr, midx-(midx/2),midy/2);
   cairo_show_text(cr, buffer);

   cairo_set_source_rgb(cr, RED);
   radius = MIN (midx, midy) -2 ;
   cairo_move_to(cr, midx , 0);

   DrawAxisZ (widget, cr, midx, midy);
   DrawAxisZ_actu (widget, cr, midx, midy);
   
   return FALSE;
}


static gboolean draw_cb_speed(GtkWidget *widget, cairo_t *cr, gpointer data)
{
   int midx = gtk_widget_get_allocated_width(widget)/2;
   int midy = gtk_widget_get_allocated_height(widget)/2;
   int nHour;

   for (nHour = 1; nHour <= 36; nHour++)
   {
        DrawTickAt (widget, cr, nHour, midx, midy);
   }

   cairo_set_source_rgb(cr, RED);
   cairo_set_font_size (cr, 30);
   cairo_move_to(cr, midx-(midx/2),midy/2);
   cairo_show_text(cr, buffer);

   cairo_set_source_rgb(cr, RED);
   radius = MIN (midx, midy) -2 ;
   cairo_move_to(cr, midx , 0);

    DrawAxisSpeed (widget, cr, midx, midy);

   return FALSE;
}


static void set_XAxis (GtkWidget *widget, gpointer data, GtkWidget *xentry) 
{ 
	GtkAdjustment  *adjustment = gtk_range_get_adjustment(widget);
   Robot_Params.XAxis = gtk_adjustment_get_value(adjustment);   
   char str[20];
   snprintf(str, 20, "%.2f deg.", Robot_Params.XAxis);
   gtk_entry_set_text(GTK_ENTRY(xentry), str);
}

static void set_YAxis (GtkWidget *widget, gpointer data, GtkWidget *yentry) 
{ 
	GtkAdjustment  *adjustment = gtk_range_get_adjustment(widget);
   Robot_Params.YAxis = gtk_adjustment_get_value(adjustment);
   char str[20];
   snprintf(str, 20, "%.2f deg.", Robot_Params.YAxis);
   gtk_entry_set_text(GTK_ENTRY(yentry), str);

}

static void set_ZAxis (GtkWidget *widget, gpointer data, GtkWidget *zentry) 
{ 
	GtkAdjustment  *adjustment = gtk_range_get_adjustment(widget);
   Robot_Params.ZAxis = gtk_adjustment_get_value(adjustment);
   char str[20];
   snprintf(str, 20, "%.2f deg.", Robot_Params.ZAxis);
   gtk_entry_set_text(GTK_ENTRY(zentry), str);
}

static void set_Speed (GtkWidget *widget, gpointer data, GtkWidget *speedentry) 
{ 
	GtkAdjustment  *adjustment = gtk_range_get_adjustment(widget);
   Robot_Params.Speed = gtk_adjustment_get_value(adjustment);
   char str[20];
   snprintf(str, 20, "%.2f %%", Robot_Params.Speed*10);
   gtk_entry_set_text(GTK_ENTRY(speedentry), str);
}

static void set_P (GtkWidget *widget, gpointer data) 
{ 
	GtkAdjustment  *adjustment = gtk_range_get_adjustment(widget);
   Robot_Params.P = gtk_adjustment_get_value(adjustment);
}

static void set_I (GtkWidget *widget, gpointer data) 
{ 
	GtkAdjustment  *adjustment = gtk_range_get_adjustment(widget);
   Robot_Params.I = gtk_adjustment_get_value(adjustment);
}

static void set_D (GtkWidget *widget, gpointer data) 
{ 
	GtkAdjustment  *adjustment = gtk_range_get_adjustment(widget);
   Robot_Params.D = gtk_adjustment_get_value(adjustment);
}

static void set_synch (GtkWidget *widget, gpointer data) 
{ 
   flag_synch = (flag_synch)?0:1;
}

static void send_params (GtkWidget *widget, gpointer data){

	char buff[MAXLEN];
	int result;
	/* Socket address structure */
	struct sockaddr_in socket_addr;
	/* Create socket */
	int my_socket = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);

	if (my_socket == -1) {
		fprintf(stderr, "Cannot create socket\n");
		return;
	}

	/* Initialize socket address to 0*/
	memset(&socket_addr, 0, sizeof(socket_addr));
	/* Set socket address parameters */
	socket_addr.sin_family = AF_INET;
	socket_addr.sin_port = htons(1100);
	result = inet_pton(AF_INET, "127.0.0.1", &socket_addr.sin_addr);
	/* Check the address translation result */
	if (result < 0) {
		fprintf(stderr, "First parameter is not a valid address family\n");
		close(my_socket);
		return;
	}
	else if (result == 0) {
		fprintf(stderr, "Second parameter does not contain valid ip address\n");
		close(my_socket);
		return;
	}

	/* Try to connect with server */
	if (connect(my_socket, (struct sockaddr *)&socket_addr, sizeof(socket_addr)) == -1) {
		fprintf(stderr, "Connect failed\n");
		close(my_socket);
		return;
	}

	/* Send a message to server */
   snprintf(buff, 39, "%05d%05d%05d%05d%04d%04d%04d%05d%01d", (int)floor(100*Robot_Params.XAxis), 
                                       (int)floor(100*Robot_Params.YAxis),
                                       (int)floor(100*Robot_Params.ZAxis),
                                       (int)floor(100*Robot_Params.Speed),
                                       (int)floor(100*Robot_Params.P),
                                       (int)floor(100*Robot_Params.I),
                                       (int)floor(100*Robot_Params.D),
                                       getpid(),
                                       flag_synch);
	send(my_socket, buff, strlen(buff), 0);

	/* Shut down the socket */
	shutdown(my_socket, SHUT_RDWR);

	/* Clean up */
	close(my_socket);

}



static gint time_handler (GtkWidget *widget)
{
    gtk_widget_queue_draw(widget);
    return TRUE;
}

static void task_handler(int);


int main_gtk(int argc, char *argv[])
{

   flag_synch = 0;
   int	status;
	/* Initialize semaphore */
	if ((status = sem_init(&mySemaphore, 0, 10))) {
		fprintf(stderr, "Error creating semaphore : %d\n", status);
		return 0;
	}

    /* Prepare sigaction struct for handler */
    struct sigaction task_action;
    task_action.sa_handler = task_handler;
    sigemptyset(&task_action.sa_mask);
    task_action.sa_flags = 0;

    /* Register signal handler for SIGRTMIN */
    if (sigaction(SIGRTMIN, &task_action, NULL) < 0) {
        fprintf(stderr, "Cannot register SIGRTMIN handler.\n");
        return 0;
    }

   color_sig.red = 1.0;
   color_sig.green = 0.0;
   color_sig.blue = 0.0;

   colo.red = 0.0;
   colo.green = 0.0;
   colo.blue = 0.8;
   colo.alpha = 0.25;

   init_receive_UDP();
   GError          *erro = NULL;
   gtk_init(&argc, &argv);

   builder = gtk_builder_new();
   gtk_builder_add_from_file (builder, "robot_arm_2.glade", &erro);

   if (erro != NULL) {
      fprintf (stderr, "Unable to read file: %s\n", erro->message);
      g_error_free(erro);
      return 1;
   }

   window = GTK_WIDGET(gtk_builder_get_object(builder, "window"));

   if (window == NULL || !GTK_IS_WINDOW(window)) {
      fprintf (stderr, "Unable to get window. (window == NULL || window != GtkWindow)\n");
      return 1;
   }

   gtk_builder_connect_signals(builder, NULL);

   buttonQuit = gtk_builder_get_object (builder, "Quit");
   g_signal_connect (buttonQuit, "clicked", G_CALLBACK (gtk_main_quit), NULL);
   
   scroll_XAxis = GTK_WIDGET(gtk_builder_get_object(builder, "X"));

   oveXEntry = GTK_WIDGET(gtk_builder_get_object(builder, "Ove_X"));
   gtk_widget_override_background_color(oveXEntry, GTK_STATE_NORMAL, &colo);
   oveYEntry = GTK_WIDGET(gtk_builder_get_object(builder, "Ove_Y"));
   gtk_widget_override_background_color(oveYEntry, GTK_STATE_NORMAL, &colo);
   oveZEntry = GTK_WIDGET(gtk_builder_get_object(builder, "Ove_Z"));
   gtk_widget_override_background_color(oveZEntry, GTK_STATE_NORMAL, &colo);

   synch = GTK_WIDGET(gtk_builder_get_object(builder, "Synch"));
   g_signal_connect (synch, "toggled", G_CALLBACK (set_synch), NULL);

   xEntry = GTK_WIDGET(gtk_builder_get_object(builder, "XEntry"));
   gtk_widget_override_background_color(xEntry, GTK_STATE_NORMAL, &colo);
   g_signal_connect (scroll_XAxis, "change-value", G_CALLBACK (set_XAxis), xEntry);
   
   scroll_YAxis = GTK_WIDGET(gtk_builder_get_object(builder, "Y"));
   yEntry = GTK_WIDGET(gtk_builder_get_object(builder, "YEntry"));
   gtk_widget_override_background_color(yEntry, GTK_STATE_NORMAL, &colo);
   g_signal_connect (scroll_YAxis, "change-value", G_CALLBACK (set_YAxis), yEntry);

   scroll_ZAxis = GTK_WIDGET(gtk_builder_get_object(builder, "Z"));
   zEntry = GTK_WIDGET(gtk_builder_get_object(builder, "ZEntry"));
   gtk_widget_override_background_color(zEntry, GTK_STATE_NORMAL, &colo);
   g_signal_connect (scroll_ZAxis, "change-value", G_CALLBACK (set_ZAxis), zEntry);
   
   scroll_Speed = GTK_WIDGET(gtk_builder_get_object(builder, "Speed"));
   speedEntry = GTK_WIDGET(gtk_builder_get_object(builder, "SpeedEntry"));
   gtk_widget_override_background_color(speedEntry, GTK_STATE_NORMAL, &colo);
   g_signal_connect (scroll_Speed, "change-value", G_CALLBACK (set_Speed), speedEntry);

   scroll_P = GTK_WIDGET(gtk_builder_get_object(builder, "P"));
   g_signal_connect (scroll_P, "change-value", G_CALLBACK (set_P), NULL);

   scroll_I = GTK_WIDGET(gtk_builder_get_object(builder, "I"));
   g_signal_connect (scroll_I, "change-value", G_CALLBACK (set_I), NULL);

   scroll_D = GTK_WIDGET(gtk_builder_get_object(builder, "D"));
   g_signal_connect (scroll_D, "change-value", G_CALLBACK (set_D), NULL);

   buttonApply = gtk_builder_get_object (builder, "Apply");
   g_signal_connect (buttonApply, "clicked", G_CALLBACK (send_params), NULL);

   drawing_area_x = GTK_WIDGET(gtk_builder_get_object(builder, "Draw_X"));
   drawing_area_y = GTK_WIDGET(gtk_builder_get_object(builder, "Draw_Y"));
   drawing_area_z = GTK_WIDGET(gtk_builder_get_object(builder, "Draw_Z"));
   drawing_area_speed = GTK_WIDGET(gtk_builder_get_object(builder, "Draw_Speed"));

   g_signal_connect (drawing_area_x, "draw", G_CALLBACK (draw_cb_X), NULL);
   g_signal_connect (drawing_area_y, "draw", G_CALLBACK (draw_cb_Y), NULL);
   g_signal_connect (drawing_area_z, "draw", G_CALLBACK (draw_cb_Z), NULL);
   g_signal_connect (drawing_area_speed, "draw", G_CALLBACK (draw_cb_speed), NULL);

   
   g_timeout_add (10, (GSourceFunc) time_handler, drawing_area_x);
   g_timeout_add (11, (GSourceFunc) time_handler, drawing_area_y);
   g_timeout_add (12, (GSourceFunc) time_handler, drawing_area_z);
   g_timeout_add (13, (GSourceFunc) time_handler, drawing_area_speed);


   g_object_unref(builder);

   gtk_widget_show(window);   
         
   gtk_main();




   pthread_barrier_wait(&barrier_GUI);
   return 0;
}

void task_handler(int sig) {
   if(color_sig.alpha < 0.5){
      color_sig.alpha = 0.7;}
   else{
      color_sig.alpha = 0.2;}
   sem_wait(&mySemaphore);
   if(Robot_Actual_Params.XAxis > 200)
      gtk_widget_override_background_color(oveXEntry, GTK_STATE_NORMAL, &color_sig);
   if(Robot_Actual_Params.YAxis > 200)
      gtk_widget_override_background_color(oveYEntry, GTK_STATE_NORMAL, &color_sig);
   if(Robot_Actual_Params.ZAxis > 200)
      gtk_widget_override_background_color(oveZEntry, GTK_STATE_NORMAL, &color_sig);
   sem_post(&mySemaphore);

}