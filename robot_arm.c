#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <mqueue.h>
#include <pthread.h>
#include <time.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <signal.h>

#include "PID.h"
#include "receive_TCPIP.h"
#include "inverse_kinematics.h"
#include "motors.h"
#include "motors.h"
#include "periodic.h"
#include "gtk.h"
#include "send_UDPIP.h"
#include "angle_guard.h"

/*
 * Main project function
 */

int main(int argc, char *argv[]) {

	/* Char that we get from keyboard*/
    char c;

	pid_t pid;
	char * child_arg[3] = {"./child_program", NULL};
	pid = fork();
	

	if (pid == 0) { /* This is child process */

		main_gtk();		
		execvp(child_arg[0], child_arg);

	} else { /* This is parent process */
	
		init_inverse_kinematics();
		init_angle_guard();
		init_send_UDPIP();
		init_receive_TCPIP();
		init_periodic();
		
		waitpid(pid, NULL, 0);
	}

	return EXIT_SUCCESS;
}

