#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <math.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <string.h>
#include <unistd.h>


#include "inverse_kinematics.h"
#include "periodic.h"
#include "motors.h"
#include "PID.h"

#define MAXLEN 100

/* Parameters values */
_set_value set_value = {0.0, 0.0, 0.0};

/* Semaphores for global variables */
pthread_mutex_t set_PID_mtr_mutex = PTHREAD_MUTEX_INITIALIZER;


/* Internal functions prototypes */
double diff_eq(double prev_output, double input);
double my_rand();

/**
 * Function initializes plant module
 */
int initialize_motors() {

	pthread_rwlock_init(&calc_mtr_ang_mutex, NULL);

	calc_mtr_ang_1_tmp = 0;
	calc_mtr_ang_2_tmp = 0;
	calc_mtr_ang_3_tmp = 0;

	printf("Motors Threat Active!\n");
	fflush(stdout);

	return 0;
}

/**
 * Function calculates one plant step
 */
int calculate_motors() {

	double motor_1, motor_2, motor_3;
	double noise;

/* Set new values calculated in previous step*/

	pthread_rwlock_wrlock(&calc_mtr_ang_mutex);
		calc_ang.mtr_1 = calc_mtr_ang_1_tmp;
		calc_ang.mtr_2 = calc_mtr_ang_2_tmp;
		calc_ang.mtr_3 = calc_mtr_ang_3_tmp;
	pthread_rwlock_unlock(&calc_mtr_ang_mutex);

	pthread_mutex_lock(&set_PID_mtr_mutex);
	double PID_input_1 = set_value.PID_mtr_1;
	double PID_input_2 = set_value.PID_mtr_2;
	double PID_input_3 = set_value.PID_mtr_3;
	pthread_mutex_unlock(&set_PID_mtr_mutex);

	/* Calculate output with Euler method */
	motor_1 = diff_eq(calc_mtr_spd_1, PID_input_1);
	motor_2 = diff_eq(calc_mtr_spd_2, PID_input_2);
	motor_3 = diff_eq(calc_mtr_spd_3, PID_input_3);

	calc_mtr_spd_1 += motors_params.ts * motor_1;
	calc_mtr_spd_2 += motors_params.ts * motor_2;
	calc_mtr_spd_3 += motors_params.ts * motor_3;

	/* Generate extra noise */
	noise = my_rand();
	
	/* Calculate of angle move */
	calc_mtr_ang_1_tmp += calc_mtr_spd_1*INTERVAL + noise;	
	calc_mtr_ang_2_tmp += calc_mtr_spd_2*INTERVAL + noise;
	calc_mtr_ang_3_tmp += calc_mtr_spd_3*INTERVAL + noise;

	calc_mtr_ang_1_tmp = (calc_mtr_ang_1_tmp<360)?(calc_mtr_ang_1_tmp):360;
	calc_mtr_ang_2_tmp = (calc_mtr_ang_2_tmp<360)?(calc_mtr_ang_2_tmp):360;
	calc_mtr_ang_3_tmp = (calc_mtr_ang_3_tmp<360)?(calc_mtr_ang_3_tmp):360;

	// send_UDP();


	return 0;
}

/**
 * Calculate differential equation of inertial plant
 */
double diff_eq(double prev_output, double input) {

	return -(1/motors_params.T) * prev_output + (motors_params.k/motors_params.T) * input;
}

/**
 * Generate my noise
 */
double my_rand() {

	double r = rand()/10000.0;
	double intpart;

	return (modf(r, &intpart)-0.5)*0.1;
}
