#ifndef GTK_H_
#define GTK_H_

#include <semaphore.h>
#include <gtk/gtk.h>

#define MAXLEN 100
#define TH1_MAX 200

extern sem_t mySemaphore;

extern pthread_barrier_t barrier_GUI;

struct robot_properties{

    double XAxis;
    double YAxis;
    double ZAxis;
    double Speed;
    double P;
    double I;
    double D;

};

struct robot_properties Robot_Actual_Params;

GtkBuilder      *builder; 
GtkWidget       *window;
GObject 	    *buttonQuit;
GObject 	    *buttonApply;
GtkScrollbar   *scroll_XAxis;
GtkScrollbar   *scroll_YAxis;
GtkScrollbar   *scroll_ZAxis;
GtkScrollbar   *scroll_Speed;
GtkScrollbar   *scroll_P;
GtkScrollbar   *scroll_I;
GtkScrollbar   *scroll_D;
GtkScrollbar   *synch;
GtkAdjustment  *adjustment;


GtkWidget *frame;
GtkWidget *drawing_area_x;
GtkWidget *drawing_area_y;
GtkWidget *drawing_area_z;
GtkWidget *drawing_area_speed;
GtkWidget *ebox;
GtkWidget *pmenu;
GtkWidget *secondsMenuItem;
GtkWidget *frameMenuItem;
GtkWidget *hideMenuItem;
GtkWidget *quitMenuItem;
GtkWidget *xEntry;
GtkWidget *yEntry;
GtkWidget *zEntry;
GtkWidget *oveXEntry;
GtkWidget *oveYEntry;
GtkWidget *oveZEntry;
GtkWidget *speedEntry;

GdkRGBA colo;
static GdkRGBA color_sig;

#define WINDOW_WIDTH  300
#define WINDOW_HEIGHT 300

#define WHITE 255,255,255
#define BLUE    0,  0,255
#define RED  0.77, 0.16, 0.13
#define BLACK   0,  0, 0
#define WIDTH 3

static char buffer[256];
static time_t curtime;
static struct tm *loctime;
static double seconds;
static int minutes;
static int hours;
static int radius;
static int movingSecondsEffect = FALSE;
static int flag_synch;
int flag_clock;



/* Function initializes periodic thread */
int main_gtk();

void DrawTickAt (GtkWidget *widget, cairo_t *cr, int nHour, int cx, int cy);

void DrawAxisX (GtkWidget *widget, cairo_t *cr, int cx, int cy);
void DrawAxisY (GtkWidget *widget, cairo_t *cr, int cx, int cy);
void DrawAxisZ (GtkWidget *widget, cairo_t *cr, int cx, int cy);
void DrawAxisSpeed (GtkWidget *widget, cairo_t *cr, int cx, int cy);

static gboolean draw_cb_X(GtkWidget *widget, cairo_t *cr, gpointer data);
static gboolean draw_cb_Y(GtkWidget *widget, cairo_t *cr, gpointer data);
static gboolean draw_cb_Z(GtkWidget *widget, cairo_t *cr, gpointer data);
static gboolean draw_cb_speed(GtkWidget *widget, cairo_t *cr, gpointer data);

void DrawAxisX_actu (GtkWidget *widget, cairo_t *cr, int cx, int cy);
void DrawAxisY_actu (GtkWidget *widget, cairo_t *cr, int cx, int cy);
void DrawAxisZ_actu (GtkWidget *widget, cairo_t *cr, int cx, int cy);

static void set_XAxis (GtkWidget *widget, gpointer data, GtkWidget *xentry);
static void set_YAxis (GtkWidget *widget, gpointer data, GtkWidget *xentry);
static void set_ZAxis (GtkWidget *widget, gpointer data, GtkWidget *xentry);
static void set_Speed (GtkWidget *widget, gpointer data, GtkWidget *xentry);

static void set_P (GtkWidget *widget, gpointer data);
static void set_I (GtkWidget *widget, gpointer data);
static void set_D (GtkWidget *widget, gpointer data);

static void send_params (GtkWidget *widget, gpointer data);
static void set_synch (GtkWidget *widget, gpointer data);

#endif /* PERIODIC_H_ */
