#ifndef RECEIVE_TCPIP_H_
#define RECEIVE_TCPIP_H_

/* Make receive_TCPIP message queue public */
extern mqd_t receive_TCPIP_MQ;

extern int chld_pid;
extern int flag_synchronous;

extern pthread_mutex_t synch_mutex;

/* Function initialize receive_TCPIP thread */
int init_receive_TCPIP();

#endif /* RECEIVE_TCPIP_H_ */
