#include <stdio.h>
#include <stdlib.h>
#include <error.h>
#include <errno.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <pthread.h>
#include <mqueue.h>
#include <time.h>

#include "inverse_kinematics.h"
#include "PID.h"

_setpoint setpoint = {0.0, 0.0, 0.0};
_motors_params motors_params = {0.1, 0.01, 0.01};
pthread_barrier_t barrier;

_robot_angles calc_inv_kin(double X, double Y, double Z){

/* TODO Implement correct inverse kinematics*/
	struct _robot_angles Angles = {X, Y, Z};
	return Angles;
}

void *tInvKineThreadFunc(void *);

/* Thread variable */
pthread_t tInvKineThread;
/* Thread attributes */
pthread_attr_t aInvKineThreadAttr;

mqd_t	receive_TCPIP_MQ;
struct	mq_attr receive_TCPIP_MQAttr;

int init_inverse_kinematics() {

	char c;
	int status;

	/* Initialize attributes structure */
	pthread_attr_init(&aInvKineThreadAttr);

	/* Create screen thread */
	if ((status = pthread_create(&tInvKineThread, &aInvKineThreadAttr, tInvKineThreadFunc, NULL))) {
		fprintf(stderr, "Cannot create thread.\n");
		return 0;
	}

		/* Set Message Queue attributes */
	receive_TCPIP_MQAttr.mq_maxmsg = 10;
	receive_TCPIP_MQAttr.mq_msgsize = 100;

	/* Create Message Queue */
	if ((receive_TCPIP_MQ = mq_open("/receive_TCPIP_MQ", O_CREAT | O_RDWR, 0666, &receive_TCPIP_MQAttr)) == -1) {
		fprintf(stderr, "Creation of the mqueues failed\n");
		return 0;
	}


	printf("Inverse Kinematics Threat Active!\n");
	fflush(stdout);
	return 0;

}

void *tInvKineThreadFunc(void *cookie) {

	/* Scheduling policy: FIFO or RR */
	int policy = SCHED_FIFO;
	/* Structure of other thread parameters */
	struct sched_param param;

	/* Read modify and set new thread priority */
	param.sched_priority = sched_get_priority_max(policy) - 2;
	pthread_setschedparam( pthread_self(), policy, &param);

    int fd_kin, fd_plan, bytes_read;
    char buff_kin[6], buff_plan[6];
	
	mqd_t mqident = mq_open ("/queue",O_RDONLY|O_CREAT , 0666, NULL);

    if (mqident == -1){
        printf("Error opening message queue. Exiting program. \n");
        printf( "Error : %s\n", strerror( errno ) );
        exit(0);
    }
    char str1[200];
    memset(str1,0,strlen(str1));
    struct mq_attr attr;

    while (1) {
    
			mq_getattr(mqident,&attr);
			ssize_t receive = mq_receive(mqident,str1,attr.mq_msgsize,0);
			if (receive == -1){
				printf("Error receiving message. Exiting program. \n");
				exit(0);
			}

			char subbuff[6];
			subbuff[5] = '\n';
			memcpy( subbuff, &str1[0], 5 );
			double XAxis = atof(subbuff)/100;
			memcpy( subbuff, &str1[5], 5 );
			double YAxis = atof(subbuff)/100;
			memcpy( subbuff, &str1[10], 5 );
			double ZAxis = atof(subbuff)/100;
			memcpy( subbuff, &str1[15], 5 );
			double Speed = atof(subbuff)/1000;
			memcpy( subbuff, &str1[20], 4 );
			double setP = atof(subbuff)/1000;
			memcpy( subbuff, &str1[24], 4 );
			double setI = atof(subbuff)/1000;
			memcpy( subbuff, &str1[28], 4 );
			double setD = atof(subbuff)/1000;

			struct _robot_angles Robot_angles = calc_inv_kin(XAxis, YAxis, ZAxis);
			
			printf("P: %f, I: %f, D: %f\n", setP, setI, setD);

			pthread_mutex_lock(&set_val_mtr_mutex);
			setpoint.mtr_1 = Robot_angles.th1;
			setpoint.mtr_2 = Robot_angles.th2;
			setpoint.mtr_3 = Robot_angles.th3;
			pthread_mutex_unlock(&set_val_mtr_mutex);

			pthread_mutex_lock(&PID_params_mutex);
			PID.P = setP;
			PID.I = setI;
			PID.D = setD;
			pthread_mutex_unlock(&PID_params_mutex);

			motors_params.k = Speed/100;

        }

	pthread_barrier_wait(&barrier);
	return 0;
}
