#ifndef PERIODIC_H_
#define PERIODIC_H_

extern pthread_mutex_t calc_mtr_ang_mutex;

static int counter = 0;

typedef struct _set_value {

	double PID_mtr_1;
	double PID_mtr_2;
	double PID_mtr_3;

}_set_value;

extern _set_value set_value;


typedef struct _calc_ang {

	double mtr_1;
	double mtr_2;
	double mtr_3;

}_calc_ang;

extern _calc_ang calc_ang;

/* Function initializes periodic thread */
int init_periodic();

#endif /* PERIODIC_H_ */
