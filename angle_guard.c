#include <stdio.h>
#include <pthread.h>
#include <mqueue.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>

#include "inverse_kinematics.h"
#include "angle_guard.h"
#include "receive_TCPIP.h"
#include "periodic.h"

#define TH1_MAX 200
#define TH2_MAX 200
#define TH3_MAX 200


/* AngleGuard thread function prototype */
void *tAngleGuardThreadFunc(void *);

/* Thread variable */
pthread_t tISRThread;
/* Thread attributes structure */
pthread_attr_t aISRThreadAttr;

int init_angle_guard() {

	int status;

	/* Initialize attributes structure */
	pthread_attr_init(&aISRThreadAttr);

	/* Start thread */
	if ((status = pthread_create(&tISRThread, NULL, tAngleGuardThreadFunc, &aISRThreadAttr))) {
		fprintf(stderr, "Cannot create thread.\n");
		return 0;
	}

	printf("Angle Guard Threat Active!\n");
	fflush(stdout);

	return 1;
}

void *tAngleGuardThreadFunc(void *cookie) {

	/* Scheduling policy: FIFO or RR */
	int policy = SCHED_FIFO;
	/* Structure of other thread parameters */
	struct sched_param param;

	/* Read modify and set new thread priority */
	param.sched_priority = sched_get_priority_max(policy) - 1;
	pthread_setschedparam( pthread_self(), policy, &param);

	while(1){

		pthread_rwlock_rdlock(&calc_mtr_ang_mutex);
		double guard_th1 = calc_ang.mtr_1;
		double guard_th2 = calc_ang.mtr_2;
		double guard_th3 = calc_ang.mtr_3;
		pthread_rwlock_unlock(&calc_mtr_ang_mutex);

		if(guard_th1 > TH1_MAX || guard_th2 > TH2_MAX || guard_th3 > TH3_MAX){
			kill(chld_pid, SIGRTMIN);
			}
		sleep(1);
	}

	pthread_barrier_wait(&barrier);
	return EXIT_SUCCESS;
}
