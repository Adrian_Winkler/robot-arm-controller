#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <signal.h>
#include <time.h>
#include <errno.h>
#include <mqueue.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <string.h>
#include <unistd.h>

#include "inverse_kinematics.h"
#include "periodic.h"
#include "motors.h"
#include "PID.h"

#define T 0.1

_calc_ang calc_ang = {0.0, 0.0, 0.0};

pthread_mutex_t calc_mtr_ang_mutex;

/* Thread function prototype */
void *tPeriodicThreadFunc(void *);


/**
 * Function starts periodic thread
 */
int init_periodic() {

	int status;

	/* Thread attributes variable */
	pthread_attr_t aPeriodicThreadAttr;
	/* Structure with time values */
	struct	itimerspec timerSpecStruct;
	/* Timer variable */
	timer_t	timerVar;
	/* Signal variable */
	struct	sigevent timerEvent;

	/* Initialize thread attributes structure */
	pthread_attr_init(&aPeriodicThreadAttr);

	/* Initialize event to create thread */
	timerEvent.sigev_notify = SIGEV_THREAD;
    timerEvent.sigev_notify_function = tPeriodicThreadFunc;
	timerEvent.sigev_notify_attributes = &aPeriodicThreadAttr;

	/* Create timer */
  	if ((status = timer_create(CLOCK_REALTIME, &timerEvent, &timerVar))) {
  		fprintf(stderr, "Error creating timer : %d\n", status);
  		return 0;
  	}

	/* Set up timer structure with time parameters */
	timerSpecStruct.it_value.tv_sec = 1;
	timerSpecStruct.it_value.tv_nsec = 0;
	timerSpecStruct.it_interval.tv_sec = 0;
	timerSpecStruct.it_interval.tv_nsec = T * 500000000;

	/* Initialize plant */
	initialize_motors();
	initialize_PID();


	/* Change timer parameters and run */
	timer_settime( timerVar, 0, &timerSpecStruct, NULL);

	printf("Periodic Threat Active!\n");
	fflush(stdout);

	return 0;
}

/*
 *  Periodic thread function
 */
void *tPeriodicThreadFunc(void *cookie) {

	/* Scheduling policy: FIFO or RR */
	int policy = SCHED_FIFO;
	/* Structure of other thread parameters */
	struct sched_param param;

	/* Read modify and set new thread priority */
	param.sched_priority = sched_get_priority_max(policy) - 3;
	pthread_setschedparam( pthread_self(), policy, &param);


	if(counter%2)
		calculate_motors();
	if(!(counter%2))
		calculate_PID();

	++counter;

	return 0;
}
