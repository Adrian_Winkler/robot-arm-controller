#include <stdio.h>
#include <pthread.h>
#include <mqueue.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <string.h>>>
#include <unistd.h>
#include <signal.h>

#include "gtk.h"

#define MAXLEN 100

/* Keyboard thread function prototype */
void *tReceiveThreadFunc(void *);

/* Thread variable */
pthread_t tISRThread;
/* Thread attributes structure */
pthread_attr_t aISRThreadAttr;




int init_receive_UDP() {
	int status;

	/* Initialize attributes structure */
	pthread_attr_init(&aISRThreadAttr);

	/* Start thread */
	if ((status = pthread_create(&tISRThread, NULL, tReceiveThreadFunc, &aISRThreadAttr))) {
		fprintf(stderr, "Cannot create thread.\n");
		return 0;
	}

	printf("Receive UDP Threat Active!\n");
	fflush(stdout);
	return 0;

}



void *tReceiveThreadFunc(void *cookie) {

	/* Scheduling policy: FIFO or RR */
	int policy = SCHED_FIFO;
	/* Structure of other thread parameters */
	struct sched_param param;

	/* Read modify and set new thread priority */
	param.sched_priority = sched_get_priority_max(policy) - 1;
	pthread_setschedparam( pthread_self(), policy, &param);

	char buff[MAXLEN], answer[MAXLEN];
	int n, addr_length;

	/* Socket address structure */
	struct sockaddr_in socket_addr;
	struct sockaddr client_addr;

	/* Create socket variable */
	int my_socket = socket(AF_INET, SOCK_DGRAM, 0);

	if(my_socket == -1) {
		fprintf(stderr, "Cannot create socket\n");
		return 0;
	}

	/* Initialize socket address to 0*/
	memset(&socket_addr, 0, sizeof(socket_addr));
	/* Set socket address parameters */
	socket_addr.sin_family = AF_INET;
	socket_addr.sin_port = htons(1100);
	socket_addr.sin_addr.s_addr = INADDR_ANY;

	/* Bind socket to socket address struct */
	if(bind(my_socket, (struct sockaddr *)&socket_addr, sizeof(socket_addr)) == -1) {
		fprintf(stderr, "Cannot bind socket\n");
		close(my_socket);
		return 0;
	}
	addr_length = sizeof(client_addr);
	for(;;) {

		/* Read what others want to tell you */    
		n = recvfrom(my_socket, (char *)buff, MAXLEN, MSG_WAITALL, &client_addr, &addr_length);  
		char get_sign[7];
		get_sign[6] = '\0';

		memcpy( get_sign, &buff[0], 5 );
		float X = atof(get_sign)/100;
		memcpy( get_sign, &buff[6], 5 );
		float Y = atof(get_sign)/100;
		memcpy( get_sign, &buff[12], 5 );
		float Z = atof(get_sign)/100;

		if(buff[5] == '-')
			X *= -1;
		if(buff[11] == '-')
			Y *= -1;
		if(buff[17] == '-')
			Z *= -1;

		sem_wait(&mySemaphore);
		Robot_Actual_Params.XAxis = X;
		Robot_Actual_Params.YAxis = Y;
		Robot_Actual_Params.ZAxis = Z;
		sem_post(&mySemaphore);

	}

	pthread_barrier_wait(&barrier_GUI);
	return EXIT_SUCCESS;
}
