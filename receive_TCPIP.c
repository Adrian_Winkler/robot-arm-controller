#include <stdio.h>
#include <pthread.h>
#include <mqueue.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <time.h>

#include "inverse_kinematics.h"
#include "receive_TCPIP.h"

#define MAXLEN 100

chld_pid = 0;
flag_synchronous = 0;

pthread_mutex_t synch_mutex;

/* receive_TCPIP thread function prototype */
void *tISRThreadFunc(void *);

/* Thread variable */
pthread_t tISRThread;
/* Thread attributes structure */
pthread_attr_t aISRThreadAttr;
/* Message Queue for string */
mqd_t	receive_TCPIP_MQ;
struct	mq_attr receive_TCPIP_MQAttr;

/**
 * receive_TCPIP thread initialization fucntion
 */
int init_receive_TCPIP() {

	int status;

	/* Initialize attributes structure */
	pthread_attr_init(&aISRThreadAttr);

	/* Start thread */
	if ((status = pthread_create(&tISRThread, NULL, tISRThreadFunc, &aISRThreadAttr))) {
		fprintf(stderr, "Cannot create thread.\n");
		return 0;
	}

	/* Set Message Queue attributes */
	receive_TCPIP_MQAttr.mq_maxmsg = 10;
	receive_TCPIP_MQAttr.mq_msgsize = 100;

	/* Create Message Queue */
	if ((receive_TCPIP_MQ = mq_open("/receive_TCPIP_MQ", O_CREAT | O_RDWR, 0666, &receive_TCPIP_MQAttr)) == -1) {
		fprintf(stderr, "Creation of the mqueues failed\n");
		return 0;
	}

	chld_pid = 0.0;
	printf("Receive TCPIP Threat Active!\n");
	fflush(stdout);

}

/**
 *  receive_TCPIP thread function
 */
void *tISRThreadFunc(void *cookie) {

	/* sign */
	char c;
	/* Scheduling policy: FIFO or RR */
	int policy = SCHED_FIFO;
	/* Structure of other thread parameters */
	struct sched_param param;

	/* Read modify and set new thread priority */
	param.sched_priority = sched_get_priority_max(policy);
	pthread_setschedparam( pthread_self(), policy, &param);


	char buff[MAXLEN], answer[MAXLEN];

	/* Socket address structure */
	struct sockaddr_in socket_addr;

	/* Create socket variable */
	int my_socket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

	if(my_socket == -1) {
		fprintf(stderr, "Cannot create socket\n");
		return 0;
	}

	/* Initialize socket address to 0*/
	memset(&socket_addr, 0, sizeof(socket_addr));
	/* Set socket address parameters */
	socket_addr.sin_family = AF_INET;
	socket_addr.sin_port = htons(1100);
	socket_addr.sin_addr.s_addr = INADDR_ANY;

	/* Bind socket to socket address struct */
	if(bind(my_socket, (struct sockaddr *)&socket_addr, sizeof(socket_addr)) == -1) {
		fprintf(stderr, "Cannot bind socket\n");
		close(my_socket);
		return 0;
	}

	/* Start to listen on created socket */
	if(listen(my_socket, 5) == -1) {
		fprintf(stderr, "Cannot start to listen\n");
		close(my_socket);
		return 0;
	}

	mqd_t mqident = mq_open("/queue", O_WRONLY|O_CREAT ,  0666, NULL);
								
	if (mqident == -1){
		printf("Error opening message queue. Exiting program. \n");
		exit(0);
	}
	char str[200];  //Keep the string that has been read from input and written on the pipe
	memset(str,0,strlen(str)); 

	for(;;) {
		/* Wait until somebody open connection with you */
		int my_connection = accept(my_socket, NULL, NULL);

		/* Check if connection is OK */
		if(my_connection < 0) {
			fprintf(stderr, "Accept connection failed\n");
			close(my_socket);
			return 0;
		} else {
			printf("I'm connected.\n");
		}

		/* Read what others want to tell you */
		int n = recv(my_connection, buff, MAXLEN, 0);
		printf("I have read: %s\n", buff);

		char subbuff[6];
		subbuff[5] = '\n';

		char flag_char[2];
		subbuff[1] = '\n';

		if(!chld_pid){
			memcpy( subbuff, &buff[32], 5 );
			chld_pid = atoi(subbuff);
		}

		memcpy( flag_char, &buff[37], 1 );
		pthread_mutex_lock(&synch_mutex);
			flag_synchronous = atoi(flag_char);	
		pthread_mutex_unlock(&synch_mutex);
		

		// printf("%d\n", flag_synchronous);

		memcpy( str, &buff, 39 );
		
		unsigned len = (unsigned) strlen(str); //Finds the length of the string
		int sent = mq_send(mqident,str,len,0);
		if (sent == -1){
			printf("Error sending message. Exiting program. \n");
			exit (0);
		}
		usleep(10L);
		memset(str,0,strlen(str));

		printf("I was send You MQ\n");

		usleep(150000);

		/* Shutdown connection */
		if (shutdown(my_connection, SHUT_RDWR) == -1) {
			fprintf(stderr, "Cannot shutdown socket\n");
			close(my_connection);
			return 0;
		}
		close(my_connection);
	}

	pthread_barrier_wait(&barrier);
	return 0;
}
